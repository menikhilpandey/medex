<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="initial-scale=1.0; maximum-scale=1.0; width=device-width;">
  <title>medex | Avoid Wastage of Medicines</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
<a href='home.php?userkey=<?php echo $_POST['userkey'] ?>' class="btn btn-info btn-lg logoutbtn"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
<div class="table-title">
<h3>Available Medicines</h3>
</div>
<table class="table-fill">
<thead>
<tr>
<th class="text-center">MEDICINE NAME</th>
<th class="text-center">DESCRIPTION</th>
<th class="text-center">EXPIRY DATE</th>
<th class="text-center">UNIT PRICE (INR)</th>
<th class="text-center">SELLER</th>
<th class="text-center">TAKE ACTION</th>
</tr>
</thead>
<tbody class="table-hover">

<?php
include('connectdb.php');
$conn    = Connect();
$userkey = $_POST['userkey'];
$medname = $_POST['MEDNAME'];
$sql = "SELECT * FROM forsale WHERE NOT(USERNAME = '$userkey') AND (MEDNAME LIKE '%$medname%') ORDER BY EXPDATE DESC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    	echo "<tr id=".$row["uniquekey"].">
				<td>".$row["MEDNAME"]."</td>
				<td><div>".$row["MEDDESC"]."</div></td>
				<td>".$row["EXPDATE"]."</td>
        <td>".$row["PRICE"]."</td>
        <td>".$row["USERNAME"]."</td>
        <td><div style='width:150px'>
        <form method='POST' action='buybackend.php'>
          <input placeholder='Select Quantity' type='number' min=1 max=".$row["QUANTITY"]." name='quantity'>
          <input type='hidden' name='userkey' value='$userkey'>
          <input type='hidden' name='seller' value='".$row["USERNAME"]."'>

          <input type='hidden' name='medname' value='".$row["MEDNAME"]."'>
          <input type='hidden' name='expdate' value='".$row["EXPDATE"]."'>
          <input type='hidden' name='price' value='".$row["PRICE"]."'>
          <input type='hidden' name='uniquekey' value='".$row["uniquekey"]."'>

          <input type='submit' name='submit' value='BUY NOW'>
        </form></div>
        </td>
			</tr>";
    }
} else {
    echo "<tr><td>No Existing Queries</td><td></td><td></td><td></td><td></td><td></td></tr>";
}
$conn->close();
?>
</tbody>
</table>
</body>
</html>


