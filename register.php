<?php

include('connectdb.php');

$conn    = Connect();

$fullname = $conn->real_escape_string($_POST['FULLNAME']);
$address = $conn->real_escape_string($_POST['ADDR']);
$pin = $conn->real_escape_string($_POST['PIN']);
$username    = $conn->real_escape_string($_POST['USERNAME']);
$pass   = $conn->real_escape_string($_POST['PASSWORD']);
$passconf = $conn->real_escape_string($_POST['PASSWORDCONF']);

if (strcmp($_POST['PASSWORD'],$_POST['PASSWORDCONF'])!=0) {
	die("Passwords do not match");
}

$query = "SELECT * FROM users WHERE LOWER(USERNAME) = '$username'";
$stmt = $conn->query($query);

if($stmt->num_rows >0) {
	die("Username already exists");
}
else {
	$md5Pass = md5($pass);
	$query = "INSERT INTO users (FULLNAME, ADDR, PIN, USERNAME,PASSWORD) VALUES ('$fullname','$address','$pin','$username','$md5Pass')";
	$stmt = $conn->query($query);
	if($conn->affected_rows == 1){
		header("Location: index.html");
	}
}
$conn->close();
?>