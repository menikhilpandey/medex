-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2017 at 04:54 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medex`
--

-- --------------------------------------------------------

--
-- Table structure for table `forsale`
--

CREATE TABLE `forsale` (
  `MEDNAME` varchar(50) NOT NULL,
  `MEDDESC` varchar(150) NOT NULL,
  `EXPDATE` date NOT NULL,
  `QUANTITY` bigint(10) NOT NULL,
  `PRICE` float DEFAULT NULL,
  `USERNAME` varchar(15) NOT NULL,
  `uniquekey` bigint(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchasehist`
--

CREATE TABLE `purchasehist` (
  `purchaseid` bigint(20) NOT NULL,
  `QUANTITY` bigint(20) NOT NULL,
  `MEDNAME` varchar(50) NOT NULL,
  `SELLER` varchar(15) NOT NULL,
  `AMOUNT` float NOT NULL,
  `BUYER` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `FULLNAME` varchar(50) NOT NULL,
  `ADDR` varchar(150) NOT NULL,
  `PIN` varchar(6) NOT NULL,
  `USERNAME` varchar(15) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forsale`
--
ALTER TABLE `forsale`
  ADD PRIMARY KEY (`uniquekey`),
  ADD KEY `uniquekey` (`uniquekey`);

--
-- Indexes for table `purchasehist`
--
ALTER TABLE `purchasehist`
  ADD PRIMARY KEY (`purchaseid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`USERNAME`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forsale`
--
ALTER TABLE `forsale`
  MODIFY `uniquekey` bigint(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchasehist`
--
ALTER TABLE `purchasehist`
  MODIFY `purchaseid` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
