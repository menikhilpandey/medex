# MedEx

MedEx is a platform which provides a solution to a problem which the healthcare industry has been facing since the beginning of medicines:<br>
The **problem of wastage of medicines** which occurs due to expiration.
We let the patients collect soon-to-expire medicines from consumers who have put those medicines for sale at reduced prices for emergency or short term use.

We have made a **LAMP stack Web Application** as well as **Android Application** for solving this problem.

## Getting Started
1. Clone the repository.<br>
    `git clone https://menikhilpandey@bitbucket.org/menikhilpandey/medex.git`<br>
2. This repository serves as web app and backend server for companion android app.<br>
3. Import medex.sql to your own mysql server.
4. Twig connectdb.php file to adjust your mysql server configurations.
5. Just open the web app on apache server using <hostname>/medex and you're good to go!

### Authors
* Nikhil Pandey
* Mohit Kumar